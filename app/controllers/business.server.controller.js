var Business = require('mongoose').model('BusinessProfile'),
    User = require('mongoose').model('User'),
    passport = require('passport');

var getErrorMessage = function(err){
	var message = ' ';
	if (err.code){
		// switch statement? 
		message = 'Something went wrong';
	}
	else {
			for (var errName in err.errors) {
			if (err.errors[errName].message)
				message = err.errors[errName].message;
		}

	}

	return message;
};


exports.renderCreateBProfile= function(req, res, next) {
	if (!req.business) {
		res.render('Business Signup', {
			title: 'Business SignUp',
			messages: req.flash('error')
		});
	}
	else {
		return res.redirect('/businessHP');
	}
};

exports.createBProfile= function(req, res, next) {
	if (!req.business) {
		var business = new Business(req.body);
		var message = null;
		// connect user to htis 
		business.save(function(err) {
			if (err) {
				var message = getErrorMessage(err);
				req.flash('error', message);
				return res.redirect('/BusinessSignup');
			}	
				// need error checkinh
				return res.redirect('/businessHP1');

		});
	}
	else {
		return res.redirect('/businessHP1');
	}
};

exports.renderBusinessHP1= function(req, res, next){
	return res.render('businesshomepage');
}

exports.createb = function(req, res, next) {	
	var business = new Business(req.body);
	business.save(function(err) {
		if (err) {
			return next(err);
		}
		else {
			res.json(business);
		}
	});
};

exports.listb = function(req, res, next) {
	Business.find({}, function(err, business) {
		if (err) {
			return next(err);
		}
		else {
			res.json(business);
		}
	});
};
