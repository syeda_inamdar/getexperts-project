(function(angular) {
  'use strict';
angular.module('BusinessForm', [])
  .controller('BFormController', ['$scope', function($scope) {
    $scope.bFormDetails = {};

    $scope.save= function(user) {
      $scope.bFormDetails = angular.copy(user);
    };

   /* $scope.reset = function(form) {
      if (form) {
        form.$setPristine();
        form.$setUntouched();
      }
      $scope.user = angular.copy($scope.master);
    };

    $scope.reset();
  }]); */
})(window.angular);
