var users = require('../../app/controllers/users.server.controller'),
    passport = require('passport');

module.exports = function(app) {
	app.route('/users').post(users.create).get(users.list);

	//:before substring means substring will be a request parameter 
	app.route('/users/:userId').get(users.read).put(users.update).delete(users.delete);

	app.param('userId', users.userByID);


	app.route('/register')
		.get(users.renderRegister)
		.post(users.register);

	app.route('/login')
		.get(users.renderLogin)
		.post(passport.authenticate('local', {
			successRedirect: '/',
			failureRedirect: '/login',
			failureFlash: true
		}));

	app.get('/logout', users.logout);


       	app.route('/expertform1').get(users.renderExpertForm1);
	app.route('/expertform2').get(users.renderExpertForm2);
	app.route('/expertform3').get(users.renderExpertForm3);
	app.route('/expertform4').get(users.renderExpertForm4);
	app.route('/expertFormReview').get(users.renderExpertFormReview);

	app.route('/businessSignUp').get(users.renderBusinessSignUp);
	//app.route('/login').get(users.renderLogin);
	app.route('/expertHP').get(users.renderExpertHP);
	app.route('/businessHP').get(users.renderBusinessHP);
	app.route('/projectForm').get(users.renderProjectCreation); 
	app.route('/expertMyProjectA').get(users.renderExpertMyProjectA);
	app.route('/businessMyProjectA').get(users.renderBusinessMyProjectA);
	app.route('/ProjectPreview').get(users.renderProjectPreview);

};
