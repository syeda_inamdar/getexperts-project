var users = require('../../app/controllers/users.server.controller'),
    business = require('../../app/controllers/business.server.controller');

module.exports = function(app){
	// this is for us to manually create users in the db using postman
	app.route('/bInfo').post(business.createb).get(business.listb);

	// this is for us to create the business profile from the form
	app.route('/createBProfile').get(business.renderCreateBProfile).post(business.createBProfile);

	app.route('/businessHP1').get(business.renderBusinessHP1);




};
